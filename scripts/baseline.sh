#!/bin/bash
sudo apt-get -y update

# setup repository for Hortonworks
sudo wget http://archive.cloudera.com/cdh5/ubuntu/trusty/amd64/cdh/archive.key -O archive.key
sudo apt-key add archive.key
#sudo wget http://archive.cloudera.com/cdh5/ubuntu/trusty/amd64/cdh/cloudera.list -O /etc/apt/sources.list.d/cloudera.list
sudo sh -c "echo 'deb [arch=amd64] http://archive.cloudera.com/cdh5/ubuntu/trusty/amd64/cdh trusty-cdh5.3.3 contrib' >> /etc/apt/sources.list.d/cloudera.list"
sudo sh -c "echo 'deb-src http://archive.cloudera.com/cdh5/ubuntu/precise/amd64/cdh trusty-cdh5.3.3 contrib' >> /etc/apt/sources.list.d/cloudera.list"
sudo cp /cri/cloudlab/cloudera/preferences.d/cloudera.pref /etc/apt/preferences.d/cloudera.pref
sudo apt-get update

# set up Java
sudo apt-get -y install openjdk-7-jdk
sudo sh -c "echo 'export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64' >> /etc/bash.bashrc"
sudo sh -c "echo 'export PATH=\$PATH:\$JAVA_HOME/bin' >> /etc/bash.bashrc"


# set up path
sudo sh -c "echo 'export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:\$HADOOP_COMMON_HOME:\$HADOOP_COMMON_HOME/lib:\$HADOOP_HDFS_HOME:\$HADOOP_HDFS_HOME/lib:\$HADOOP_MAPRED_HOME:\$HADOOP_MAPRED_HOME/lib:\$HADOOP_YARN_HOME:\$HADOOP_YARN_HOME/lib:\$JAVA_HOME/jre/lib/amd64:\$JAVA_HOME/jre/lib/amd64/server' >> /etc/bash.bashrc"

sudo sh -c "echo 'export JAVA_LIBRARY_PATH=\$JAVA_LIBRARY_PATH:\$HADOOP_COMMON_HOME:\$HADOOP_COMMON_HOME/lib:\$HADOOP_HDFS_HOME:\$HADOOP_HDFS_HOME/lib:\$HADOOP_MAPRED_HOME:\$HADOOP_MAPRED_HOME/lib:\$HADOOP_YARN_HOME:\$HADOOP_YARN_HOME/lib' >> /etc/bash.bashrc"

sudo sh -c "echo 'export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local/lib/pkgconfig' >> /etc/bash.bashrc"
