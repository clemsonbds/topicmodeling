import csv
import collections
from collections import defaultdict, Counter

count = 0
counter = collections.Counter()
columns = defaultdict(list)

#Removes the first column from within the file
#leaves only the values for the year
with open('chemistryyearbreakdown.txt') as input_file:
	reader = csv.reader(input_file, delimiter=' ')
	for row in reader:
		count += int(row[1])
		print count
